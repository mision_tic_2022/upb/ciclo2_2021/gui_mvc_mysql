package com.formulario.persona.controlador;

import java.util.ArrayList;

import com.formulario.persona.modelo.dao.PersonaDao;
import com.formulario.persona.modelo.vo.PersonaVo;

public class ControladorPersona {

    /**************
     * Atributos
     *************/
    private PersonaDao personaDao;

    //Constructor
    public ControladorPersona(){
        this.personaDao = new PersonaDao();
    }


    public ArrayList<PersonaVo> getPersonas() throws Exception{
        return this.personaDao.getPersonas();
    }

    public boolean insertar_persona(String nombre, String apellido, String email, int edad) throws Exception{
        //Construir objeto de tipo PersonaVo a partir de los datos recibidos desde la vista
        PersonaVo objPersona = new PersonaVo(nombre, apellido, email, edad);
        //Enviar el objeto al DAO
        return this.personaDao.insertar_persona(objPersona);
    }

    
}
