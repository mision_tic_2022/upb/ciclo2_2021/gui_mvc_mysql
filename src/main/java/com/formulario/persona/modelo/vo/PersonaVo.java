package com.formulario.persona.modelo.vo;

/**********************************************
 * Clase que representa un VO (Value Object)
 * es decir, una entidad del mundo real que 
 * igualmente está representado como una tabla 
 * en la base de datos
 ***********************************************/
public class PersonaVo {
    /*************
     * Atributos
     *************/
    private int id;
    private String nombre;
    private String apellido;
    private String email;
    private int edad;

    //Constructores
    
    public PersonaVo() {
    }

    public PersonaVo(String nombre, String apellido, String email, int edad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.edad = edad;
    }

    public PersonaVo(int id, String nombre, String apellido, String email, int edad) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.edad = edad;
    }

    

    /***********************
     * Métodos consultores 
     * y modificadores
     * ***********************/

    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public String getNombre() {
        return nombre;
    }


    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public String getApellido() {
        return apellido;
    }


    public void setApellido(String apellido) {
        this.apellido = apellido;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public int getEdad() {
        return edad;
    }


    public void setEdad(int edad) {
        this.edad = edad;
    }

}
