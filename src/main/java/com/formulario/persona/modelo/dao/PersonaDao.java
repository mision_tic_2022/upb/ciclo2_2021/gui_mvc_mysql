package com.formulario.persona.modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.formulario.persona.jdbcUtilities.JDBCUtilities;
import com.formulario.persona.modelo.vo.PersonaVo;

/*********************************************************
 * Clase que me representa un DAO (Data Access Object)
 * esta clase me representa las consultas SQL realizadas
 * a la tabla 'usuario' de la BD
 **********************************************************/


public class PersonaDao {

    public ArrayList<PersonaVo> getPersonas() throws Exception{

        Connection conn = JDBCUtilities.conectarBD();
        //Objeto para preparar consultas sql
        Statement statement = conn.createStatement();
        //Generar/representar la consulta sql
        String query = "SELECT * FROM Persona";
        //Ejecutar query y obtener una respuesta
        ResultSet respuesta = statement.executeQuery(query);
        //Crear array que contendrá la respuesta representada en objetos
        ArrayList<PersonaVo> personas = new ArrayList<PersonaVo>();
        //Recorrer el resultSet
        while(respuesta.next()){
            //Construir objeto que representa cada fila de la consulta retornada
            PersonaVo objPersona = new PersonaVo();
            //Setear los atributos del objeto con los resultados del query
            objPersona.setId( respuesta.getInt("id") );
            objPersona.setNombre( respuesta.getString("nombre") );
            objPersona.setApellido( respuesta.getString("apellido") );
            objPersona.setEmail( respuesta.getString("email") );
            objPersona.setEdad( respuesta.getInt("edad") );
            //agregar el objeto con valor al arreglo
            personas.add(objPersona);
        }
        //cerrar conexión a la BD
        conn.close();
        statement.close();

        return personas;
    }



    public boolean insertar_persona(PersonaVo personaVo) throws Exception{

        Connection conn = JDBCUtilities.conectarBD();
        String query = "INSERT INTO Persona(nombre, apellido, email, edad) VALUES(?, ?, ?, ?)";
        PreparedStatement pStatement = conn.prepareStatement(query);
        //setear los signos de interrogación por valores
        pStatement.setString(1, personaVo.getNombre());
        pStatement.setString(2, personaVo.getApellido());
        pStatement.setString(3, personaVo.getEmail());
        pStatement.setInt(4, personaVo.getEdad());

        if(pStatement.executeUpdate() == 1){
            return true;
        }else{
            return false;
        }

    }

    
}
