package com.formulario.persona.vista;

import java.util.ArrayList;
import java.util.Scanner;

import com.formulario.persona.controlador.ControladorPersona;
import com.formulario.persona.modelo.vo.PersonaVo;

public class VistaConsola {
    /*************
     * Atributos
     *************/
    private ControladorPersona cPersona;

    //Constructor
    public VistaConsola(){
        this.cPersona = new ControladorPersona();
    }

    public void mostrar_personas(){
        try {
            //Obtener todas las personas del controlador
            ArrayList<PersonaVo> personas = this.cPersona.getPersonas();
            //Recorrer el array y mostrarlo en consola
            for(int i = 0; i < personas.size(); i++){  
                //Acceder al objeto en la posicion i (0, 1, 2, 3, 4 ...)
                PersonaVo objPersona = personas.get(i);             
                String info = "Nombre: "+objPersona.getNombre();
                info += "\tApellido: "+objPersona.getApellido();
                info += "\tEmail: "+objPersona.getEmail();
                info += "\tEdad: "+objPersona.getEdad();
                //Mostrar la información en consola
                System.out.println(info);
            }

        } catch (Exception e) {
            //TODO: handle exception
            System.out.println(e);
        }
        
    }

    public void registrar_persona(){
        try (Scanner entrada = new Scanner(System.in)){
            /********************************************
             * Solicitar los datos por consola
             *******************************************/

            System.out.println("Por favor ingrese el nombre: ");
            String nombre = entrada.nextLine();

            System.out.println("Por favor ingrese el apellido: ");
            String apellido  = entrada.nextLine();

            System.out.println("Por favor ingrese el email: ");
            String email = entrada.nextLine();

            System.out.println("Por favor ingrese la edad: ");
            int edad = entrada.nextInt();

            //Enviar los datos capturados al controlador
            boolean band = this.cPersona.insertar_persona(nombre, apellido, email, edad);
            if(band){
                System.out.println("¡Persona registrada con éxito!");
            }else{
                System.out.println("Por favor intente mas tarde...");
            }

        } catch (Exception e) {
            //TODO: handle exception
            System.err.println(e);
        }
    }

}
