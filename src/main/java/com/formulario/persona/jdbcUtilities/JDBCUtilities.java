package com.formulario.persona.jdbcUtilities;

import java.sql.Connection;
import java.sql.DriverManager;

/********************************************
 * Clase manejadora de la conexión a la BD
 ********************************************/
public class JDBCUtilities {
    //url -> jdbc:mysql://url/baseDeDatos
    //de manera local-> //localhost:3306
    private static final String url = "jdbc:mysql://mintic-bd-pruebas.cc6nsjnfdbu9.us-east-2.rds.amazonaws.com/misiontic";
    private static final String user = "admin";
    private static final String password = "misiontic2022";

    public static Connection conectarBD() throws Exception{
        return DriverManager.getConnection(url, user, password);
    }    
}
