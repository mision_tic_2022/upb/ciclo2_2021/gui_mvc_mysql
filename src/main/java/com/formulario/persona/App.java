package com.formulario.persona;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.formulario.persona.jdbcUtilities.JDBCUtilities;
import com.formulario.persona.vista.VistaConsola;


public class App 
{
    public static void main( String[] args )
    {
        
        VistaConsola vistaConsola = new VistaConsola();
        vistaConsola.mostrar_personas();
        //vistaConsola.registrar_persona();
       
    }

    /*
    public void ejemplo_conexion(){
        try {
            //Obtenemos la conexión
            Connection conn = JDBCUtilities.conectarBD();
            //Objeto para ejecutar querys
            Statement statement = conn.createStatement();
            //Ejecutar query y obtener datos
            ResultSet query = statement.executeQuery("SELECT * FROM usuario");
            //verificar y acceder a la información
            if(query.next()){
                int id = query.getInt("id");
                String nombre = query.getString("nombre");
                System.out.println("id: "+id+"\nnombre: "+nombre);
            }

        } catch (Exception e) {
            //TODO: handle exception
            System.err.println(e);
        }
    }
    */
}
